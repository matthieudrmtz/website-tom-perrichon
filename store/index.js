export const state = () => ({
    layout: true,
 })
     
 export const mutations = {
   CHANGE_NAV_LAYOUT(state, layout) {
     state.layout = layout;
   }
 }